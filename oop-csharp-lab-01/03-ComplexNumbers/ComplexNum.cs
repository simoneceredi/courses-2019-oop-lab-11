﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(this.Re * this.Re + this.Im * this.Im);
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.Re, -this.Im);
            }
        }

        public void Invert()
        {
            double div = this.Re * this.Re + this.Im * this.Im;
            this.Re /= div;
            this.Im = (-this.Im)/div;
        }

        public static ComplexNum operator +(ComplexNum c1, ComplexNum c2) => 
            new ComplexNum(c1.Re + c2.Re, c1.Im + c2.Im);
        

        public static ComplexNum operator -(ComplexNum c1, ComplexNum c2) => 
            new ComplexNum(c1.Re - c2.Re, c1.Im - c2.Im);
        

        public static ComplexNum operator *(ComplexNum c1, ComplexNum c2) => 
            new ComplexNum(c1.Re * c2.Re - c1.Im * c2.Im, c1.Im * c2.Re + c1.Re * c2.Im);
        

        public static ComplexNum operator /(ComplexNum c1, ComplexNum c2)
        {
            double div = c2.Re * c2.Re + c2.Im * c2.Im;
            return new ComplexNum((c1.Re * c2.Re + c1.Im * c2.Im) / div, (c2.Re * c1.Im - c1.Re * c2.Im) / div);
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return $"{this.Re} + i{this.Im}";
        }
    }
}
